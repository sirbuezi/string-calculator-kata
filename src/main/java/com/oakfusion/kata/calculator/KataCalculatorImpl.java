package com.oakfusion.kata.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class KataCalculatorImpl implements KataCalculator {

    private static final String COMMA = ",";
    private static final String EMPTYSTR = "";
    private static final String NEW_LINE = "\n";
    private static final String CUST_DELIMITER_REGEX = "\\/\\/\\[(.+)\\]\\n((\\d+\\1*)+\\d*)";
    private static final String MULTIPLE_DELIMITERS = "\\/\\/((\\[.+\\]){2,})\\n(.*)";
    private static final String DOUBLE_SLASH = "//";
    private static final String PIPE = "|";
    private static final String WHITESPACE = " ";

    public int add(String numbers) {
        if (null == numbers || numbers.isEmpty()) {
            return 0;
        } else {
            List<Integer> numbersAsInt = trimAndConvertStringNumbers(numbers);
            List<Integer> negatives = numbersAsInt.stream().filter(n -> n < 0).collect(Collectors.toList());
            if (!negatives.isEmpty()) {
                throw new IllegalArgumentException("You're not allowed to use negative numbers! " + negatives.toString());
            }
            return numbersAsInt.stream().filter(num -> num <= 1000).collect(Collectors.summingInt(Integer::intValue));
        }
    }

    private List<Integer> trimAndConvertStringNumbers(String numbers) {
        String[] splitNumbers;

        if (hasMultipleDelimiters(numbers)) {
            splitNumbers = splitUsingMultipleCustomDelimiters(numbers);
        } else if (hasCustomDelimiter(numbers)) {
            splitNumbers = splitUsingCustomDelimiter(numbers);
        } else {
            splitNumbers = numbers.split(COMMA + "|" + NEW_LINE);
        }
        List<String> numbersAsStrings =
                new ArrayList<>(Arrays.asList(splitNumbers));

        return numbersAsStrings.stream().map(Integer::parseInt).collect(Collectors.toList());
    }

    private boolean hasCustomDelimiter(String numbers) {
        return numbers.startsWith(DOUBLE_SLASH);
    }

    private boolean hasMultipleDelimiters(String numbers) {
        return numbers.matches(MULTIPLE_DELIMITERS);
    }

    private String[] splitUsingCustomDelimiter(String numbers) {
        Pattern pattern = Pattern.compile(CUST_DELIMITER_REGEX);
        Matcher matcher = pattern.matcher(numbers);

        if (matcher.matches()) {
            String customDelimiterSubstring = matcher.group(1);
            String numbersSubstring = matcher.group(2);
            return numbersSubstring.split(Pattern.quote(customDelimiterSubstring));
        }
        return null;
    }

    private String[] splitUsingMultipleCustomDelimiters(String numbers) {
        Pattern pattern = Pattern.compile(MULTIPLE_DELIMITERS);
        Matcher matcher = pattern.matcher(numbers);

        if (matcher.matches()) {
            String multipleDelimitersSubgroups = matcher.group(1);
            multipleDelimitersSubgroups = multipleDelimitersSubgroups
                    .replaceAll("\\]\\[", WHITESPACE)
                    .replaceFirst("\\[", EMPTYSTR)
                    .replaceFirst("\\]", EMPTYSTR);

            String[] delimiters = multipleDelimitersSubgroups.split("\\s");

            for (int delimiter = 0; delimiter < delimiters.length; delimiter++) {
                delimiters[delimiter] = Pattern.quote(delimiters[delimiter]);
            }
            multipleDelimitersSubgroups = String.join(PIPE, delimiters);
            String numbersSubstring = matcher.group(3);
            return numbersSubstring.split(multipleDelimitersSubgroups);
        }
        return null;
    }
}
