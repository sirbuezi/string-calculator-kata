package com.oakfusion.kata.calculator;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class KataCalculatorTest {

    private KataCalculatorImpl kataCalculator;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Before
    public void initCalculator() {
        kataCalculator = new KataCalculatorImpl();
    }

    // =========================== ITERATION 1 ==============================
    @Test
    public void givenEmptyString_whenAdd_thenReturnZero() {
        int result = kataCalculator.add("");
        assertEquals(0, result);
    }

    @Test
    public void givenOnlyOneNumber_whenAdd_thenReturnTheNumber() {
        int result = kataCalculator.add("20");
        assertEquals(20, result);
    }

    @Test
    public void givenTwoNumbers_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("20,30");
        assertEquals(50, result);
    }

    // =========================== ITERATION 2 ==============================
    @Test
    public void given5Numbers_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("1,4,5,20,30");
        assertEquals(60, result);
    }

    @Test
    public void given10Numbers_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("1,4,5,20,30,7,3,12,8,13");
        assertEquals(103, result);
    }

    @Test
    public void given15Numbers_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("1,4,5,20,30,7,3,12,8,13,7,3,91,4,5");
        assertEquals(213, result);
    }

    // =========================== ITERATION 3 ==============================
    @Test
    public void givenNewLineCharAsDelimiter_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("1\n4\n5");
        assertEquals(10, result);
    }

    @Test
    public void givenNewLineAndCommaDelimiters_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("1\n4,5");
        assertEquals(10, result);
    }

    // =========================== ITERATION 4 ==============================

    @Test
    public void givenCustomCharAsDelimiter_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[_]\n15_45_5");
        assertEquals(65, result);
    }

    @Test
    public void givenCustomDelimiterAsReservedRegexChar_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[+]\n15+45+5");
        assertEquals(65, result);
    }

    // =========================== ITERATION 5 ==============================

    @Test
    public void givenNegativeNumber_whenAdd_thenExpectException() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("You're not allowed to use negative numbers! [-30]");
        kataCalculator.add("20,-30");
    }

    @Test
    public void givenMultipleNegativeNumbers_whenAdd_thenExpectException() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage("You're not allowed to use negative numbers! [-30, -9, -6]");
        kataCalculator.add("20,-30,23,-9,-6");
    }

    // =========================== ITERATION 6 ==============================

    @Test
    public void givenSomeOfTheNumbersBiggerThan1000_whenAdd_thenExpectThemToBeIgnored() {
        int result = kataCalculator.add("1,4000,5,1001,1000");
        assertEquals(1006, result);
    }

    // =========================== ITERATION 7 ==============================

    @Test
    public void givenDelimiterOfAnyLength_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[;;;]\n15;;;45;;;5");
        assertEquals(65, result);
    }

    @Test
    public void givenCustomDelimiterOfAnyLengthAsReservedRegexChar_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[+++++]\n15+++++45+++++5");
        assertEquals(65, result);
    }

    // =========================== ITERATION 8 ==============================

    @Test
    public void givenMultipleCustomDelimiters_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[_][;][?]\n15?45;5_4;9");
        assertEquals(78, result);
    }

    @Test
    public void givenMultipleCustomDelimitersAsReservedRegexChar_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[*][+]\n15*45+5*4+9");
        assertEquals(78, result);
    }

    // =========================== ITERATION 9 ==============================

    @Test
    public void givenMultipleCustomDelimitersOfAnyLengthVer1_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[***][++++]\n15***45++++5***4++++9");
        assertEquals(78, result);
    }

    @Test
    public void givenMultipleCustomDelimitersOfAnyLengthVer2_whenAdd_expectCorrectSum() {
        int result = kataCalculator.add("//[***][++++][;;;][&&][!!!!!!]\n15***45++++5;;;4&&9!!!!!!7");
        assertEquals(85, result);
    }
}
